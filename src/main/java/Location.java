public class Location {
    private String citta;
    private String nomeLoc;
    private String indirizzo;

    public Location(String citta, String nomeLoc, String indirizzo) {
        this.citta = citta;
        this.nomeLoc = nomeLoc;
        this.indirizzo = indirizzo;
    }

    public String getCitta() {
        return citta;
    }

    public String getNomeLoc() {
        return nomeLoc;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public void setNomeLoc(String nomeLoc) {
        this.nomeLoc = nomeLoc;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }
}
