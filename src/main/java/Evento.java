import java.time.LocalDate;
import java.time.LocalDateTime;

public class Evento {
    private String nome;
    private String descrizioneEvento;
    private LocalDate data;
    private String oraInizioEvento;
    private int nPartecipanti;

    public Evento(String nome, String descrizioneEvento, LocalDate data, String oraInizioEvento, int nPartecipanti) {
        this.nome = nome;
        this.descrizioneEvento = descrizioneEvento;
        this.data = data;
        this.oraInizioEvento = oraInizioEvento;
        this.nPartecipanti = nPartecipanti;
    }

    public String getNome() {
        return nome;
    }

    public String getDescrizioneEvento() {
        return descrizioneEvento;
    }

    public LocalDate getData() {
        return data;
    }

    public String getOraInizioEvento() {
        return oraInizioEvento;
    }

    public int getnPartecipanti() {
        return nPartecipanti;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDescrizioneEvento(String descrizioneEvento) {
        this.descrizioneEvento = descrizioneEvento;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public void setOraInizioEvento(String oraInizioEvento) {
        this.oraInizioEvento = oraInizioEvento;
    }

    public void setnPartecipanti(int nPartecipanti) {
        this.nPartecipanti = nPartecipanti;
    }
}
