import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.util.Scanner;

public class Main {

    //inizializzo un oggetto di tipo logger da log4j, lo suo in sostituzione del sout
    protected static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //creo la stringa che rappresenta l'URL di connessione al DB in formato jdbc
        String url = "jdbc:mysql://localhost:3306/GestioneEventi";
        String username = "root"; //username db
        String password = "root"; //password db
        Class.forName("com.mysql.cj.jdbc.Driver"); //Carico il driver del db

        //con un try with resource inizializzo la connessione al db, creando un oggetto di tipo Connection
        //uso il try with resource per chiudere la connessione in automatico
        try (Connection con = DriverManager.getConnection(url, username, password)) {
            logger.info("Sono connesso al db !");

            //creo uno scanner per far interagire gli utenti con il sistema
            Scanner scanner = new Scanner(System.in);

            System.out.println("Ciao. Sei già registrato a sistema? (si/no)");
            String registrato = scanner.nextLine();

            Utente utente = null;

            if (registrato.equals("si")){
                System.out.println("OK, fai la login. Inserisci di seguito nome e password.");
                System.out.println("Username:");
                String usernameUser = scanner.nextLine();
                System.out.println("Password:");
                String upasswordUser = scanner.nextLine();

                //creo un oggetto di tipo Statement che servirà per eseguire le query
                try(Statement st = con.createStatement()){
                    // uso \" per fare l'escape, cioè per far si che vengano inseriti i caratteri speciali
                    String query = "select * from Utente where email = \"" + usernameUser + "\" and password = \"" + upasswordUser + "\" ;"; //creo una stringa contenente la query da eseguire
                    ResultSet recuperoUtente = st.executeQuery(query); //eseguo la query e la metto in un oggetto di tipo ResultSet

                    //se esiste almeno 1 risultato recupero tutte le info da db
                    if (recuperoUtente.next()){
                         utente = new Utente (
                            recuperoUtente.getString("nome"),
                            recuperoUtente.getString("cognome"),
                            recuperoUtente.getString("email"),
                            recuperoUtente.getString("password"),
                            recuperoUtente.getBoolean("isAdmin"),
                            recuperoUtente.getInt("id"));
                    } else {
                        System.out.println("Non risulta nessun utente con queste credenziali.");
                        System. exit(0);
                    }
                }
            } else {
                System.out.println("Ok, procediamo con la registrazione, inserisci prima la tua email.");
                System.out.println("Email: ");
                String newUserEmail = scanner.nextLine();
                System.out.println("Password: ");
                String newUserPassword = scanner.nextLine();
                System.out.println("Nome: ");
                String newUserNome = scanner.nextLine();
                System.out.println("Cognome: ");
                String newUserCognome = scanner.nextLine();
                System.out.println("Sei un organizzatore? (si/no)");
                String rispostaOrganizzatore = scanner.nextLine();

                boolean newUserAdmin = rispostaOrganizzatore.equals("si");

                try (Statement st = con.createStatement()) {
                    String query = "insert into Utente (nome, cognome, email, password, isAdmin) values (\"" + newUserNome + "\",\"" + newUserCognome + "\",\"" + newUserEmail + "\",\"" + newUserPassword + "\"," + newUserAdmin + ");"; //creo una stringa contenente la query da eseguire
                    st.executeUpdate(query); //eseguo la query e la metto in un oggetto di tipo ResultSet

                    System.out.println("Ti sei correttamente registrato !");
                }

                //la prima query sopra effettua l'inserimento di un nuovo utente nel db, questa invece recupera l'utente appena inserito
                //per recuperare anche il suo id.
                try(Statement st = con.createStatement()){
                    String query = "select * from Utente where email = \"" + newUserEmail + "\" and password = \"" + newUserPassword + "\" ;"; //creo una stringa contenente la query da eseguire
                    ResultSet recuperoUtente = st.executeQuery(query); //eseguo la query e la metto in un oggetto di tipo ResultSet

                    //se esiste almeno 1 risultato recupero tutte le info da db
                    if (recuperoUtente.next()){
                        utente = new Utente (
                                recuperoUtente.getString("nome"),
                                recuperoUtente.getString("cognome"),
                                recuperoUtente.getString("email"),
                                recuperoUtente.getString("password"),
                                recuperoUtente.getBoolean("isAdmin"),
                                recuperoUtente.getInt("id"));
                    } else {
                        System.out.println("Non risulta nessun utente con queste credenziali.");
                        System. exit(0);
                    }
                }
            }
            if(utente.isAdmin()){
                System.out.println("Che tipo di operazione vuoi fare sugli eventi? (crea/modifica/visualizza)");

                String rispostaOrganizzatore = scanner.nextLine();

                switch (rispostaOrganizzatore){
                    case "crea":
                        System.out.println("Ok, dammi tutte le info dell'evento.");
                        System.out.println("Nome: ");
                        String nomeEvento = scanner.nextLine();
                        System.out.println("Descrizione: ");
                        String descrizioneEvento = scanner.nextLine();
                        System.out.println("Tipologia evento: ");
                        String tipoEvento = scanner.nextLine();
                        System.out.println("Data dell'evento: ");
                        String dataEvento = scanner.nextLine();
                        System.out.println("Numero dei partecipanti: ");
                        String nPartecipantiEvento = scanner.nextLine();

                        try (Statement st = con.createStatement()) {
                            String query = "insert into Evento (nome, descrizione, tipo, data, numero_posti) " +
                                    "values (\"" + nomeEvento + "\",\"" + descrizioneEvento + "\",\"" + tipoEvento + "\",\"" + dataEvento + "\"," +  nPartecipantiEvento + ");"; //creo una stringa contenente la query da eseguire
                            st.executeUpdate(query); //eseguo la query e la metto in un oggetto di tipo ResultSet
                        }
                        break;
                    case "modifica":
                        System.out.println("Ok, dammi il nome dell'evento da modificare.");
                        String nomeEventoDaMod = scanner.nextLine();
                        System.out.println("Cosa vuoi modificare? (inserisci il nome della colonna)");
                        String cosaDaModificare = scanner.nextLine();
                        System.out.println("Qual è il nuovo valore?");
                        String nuovoValore = scanner.nextLine();

                        try (Statement st = con.createStatement()) {
                            String query = "update Evento set " + cosaDaModificare + " = \"" + nuovoValore +
                                    "\" where nome = \"" + nomeEventoDaMod + "\";";
                            st.executeUpdate(query); //eseguo la query e la metto in un oggetto di tipo ResultSet
                        }
                        break;
                    case "visualizza":
                        try (Statement st = con.createStatement()) {
                            String query = "select * from Evento;";
                            ResultSet risultati = st.executeQuery(query);
                            while(risultati.next()){
                                System.out.println("nome: " + risultati.getString("nome"));
                                System.out.println("descrizione: " + risultati.getString("descrizione"));
                                System.out.println("tipologia evento: " + risultati.getString("tipo"));
                                System.out.println("data dell'evento: " + risultati.getDate("data"));
                                System.out.println("orario di inizio evento: " + risultati.getString("orario_inizio"));
                                System.out.println("numero di partecipanti all'evento: " + risultati.getString("numero_posti"));
                            }
                        }
                        break;
                    default:
                        System.out.println("Operazione non consentita.");
                }
            } else {
                System.out.println("Questi sono gli eventi disponibili:");
                try (Statement st = con.createStatement()) {
                    String query = "select Evento.nome, Evento.id, Evento.descrizione from Evento left join Biglietto on Evento.id = Biglietto.id_evento where Evento.numero_posti > (select count(*) from Biglietto where id_evento = Evento.id);";
                    ResultSet risultati = st.executeQuery(query);
                   while(risultati.next()){
                       System.out.println("Nome dell'evento: " + risultati.getString("nome") + " codice dell'evento : " + risultati.getString("id") + " la descrizione dell'evento: " + risultati.getString("descrizione"));
                   }
                    System.out.println("Per quale evento vuoi comprare un biglietto? (inserisci id dell'evento)");
                    String idBiglietto = scanner.nextLine();

                    try (Statement st2 = con.createStatement()) {
                        String query2 = "insert into Biglietto (id_evento,id_utente,costo_biglietto) values (" + idBiglietto + "," + utente.getId() + "," + "10);";
                        st2.executeUpdate(query2);
                        System.out.println("Hai correttamente acquistato il biglietto.");
                    }
                }
            }
        }
    }
}
